package com.dans.demo.models;

import javax.persistence.*;

// We also need to add some rows into roles table before assigning any role to User.
// Run following SQL insert statements:

// INSERT INTO roles(name) VALUES('ROLE_USER');
// INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
// INSERT INTO roles(name) VALUES('ROLE_ADMIN');

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

    public Role() {

    }

    public Role(ERole name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ERole getName() {
        return name;
    }

    public void setName(ERole name) {
        this.name = name;
    }
}